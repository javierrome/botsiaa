using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;

namespace Microsoft.BotBuilderSamples.Dialogs
{
    public class SatisfaccionDialog : CancelAndHelpDialog
    {
        private const string StepMsgText = "¿Le ha sido esta información de utilidad? Puntúe con un número de 0 a 10";

        // Dependency injection uses this constructor to instantiate the dialog
        public SatisfaccionDialog()
            : base(nameof(SatisfaccionDialog))
        {
            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ConfirmPrompt(nameof(ConfirmPrompt)));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                MensajeStepAsync,
                NotaStepAsync,
            }));

            // The initial child Dialog to run.
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> MensajeStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {

            var promptMessage = MessageFactory.Text(StepMsgText, StepMsgText, InputHints.ExpectingInput);
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);

        }

        private async Task<DialogTurnResult> NotaStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            string puntuacion = stepContext.Result.ToString();

            mongoAPI.postPunctuation(stepContext.Context.Activity.From.Id, puntuacion, stepContext.Context.Activity.ChannelId);

            var promptMessage = MessageFactory.Text("Gracias", "Gracias", InputHints.ExpectingInput);
            await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);

            return await stepContext.EndDialogAsync(null, cancellationToken);
        }

        
    }
}
