using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;

namespace Microsoft.BotBuilderSamples.Dialogs
{
    public class MainDialog : ComponentDialog
    {
        private readonly SIAARecognizer _luisRecognizer;
        protected readonly ILogger Logger;
        private ContextDictionary cont;
        private string intent;
        private bool prediction = false;


        // Dependency injection uses this constructor to instantiate MainDialog
        public MainDialog(SIAARecognizer luisRecognizer, MatriculaDialog matriculaDialog, ACFDialog acfDialog, BecasDialog becasDialog,
            CompensacionDialog compensacionDialog, ContinuacionDialog continuacionDialog, EBAUDialog ebauDialog, PAU25Dialog pau25Dialog, PAU45Dialog pau45Dialog,
            PAU40Dialog pau40Dialog, PermanenciaDialog permanenciaDialog, PreinscripcionDialog preinscripcionDialog, SimultaneidadDialog simultaneidadDialog, TFGDialog tfgDialog, SatisfaccionDialog satisfaccionDialog, ILogger<MainDialog> logger)
            : base(nameof(MainDialog))
        {
            _luisRecognizer = luisRecognizer;
            Logger = logger;

            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(matriculaDialog);
            AddDialog(acfDialog);
            AddDialog(becasDialog);
            AddDialog(compensacionDialog);
            AddDialog(continuacionDialog);
            AddDialog(ebauDialog);
            AddDialog(pau25Dialog);
            AddDialog(pau45Dialog);
            AddDialog(pau40Dialog);
            AddDialog(permanenciaDialog);
            AddDialog(preinscripcionDialog);
            AddDialog(simultaneidadDialog);
            AddDialog(tfgDialog);
            AddDialog(satisfaccionDialog);
            //Conversation flow definition with tasks
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                IntroStepAsync,
                ActStepAsync,
                AnswerAsync,
                PredictionAsync,
                AnswerAsync,
                FinalStepAsync,
            }));

            // The initial child Dialog to run.
            InitialDialogId = nameof(WaterfallDialog);
        }

        

        //Check if LUIS is configured, if context exists for user or create it, and show the first messagge of the conversarion
        private async Task<DialogTurnResult> IntroStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var messageText = " ";
            if (!_luisRecognizer.IsConfigured)
            {
                await stepContext.Context.SendActivityAsync(
                    MessageFactory.Text("NOTE: LUIS is not configured. To enable all capabilities, add 'LuisAppId', 'LuisAPIKey' and 'LuisAPIHostName' to the appsettings.json file.", inputHint: InputHints.IgnoringInput), cancellationToken);

                return await stepContext.NextAsync(null, cancellationToken);
            }

            cont = ContextDictionary.Instance;

            if (!cont.usersContext.ContainsKey(stepContext.Context.Activity.From.Id))
            {
                cont.usersContext.Add(stepContext.Context.Activity.From.Id, new Context());
                cont.usersContext[stepContext.Context.Activity.From.Id].Time = DateTime.Now;
                messageText = stepContext.Options?.ToString() ?? "Bienvenido al Bot de la \"Sección de Información y Atención Administrativa\" de la Universidad de Extremadura ¿En qué puedo ayudarte?";
            }
            else
            {
                //Check the context age and reset it
                TimeSpan duration = (TimeSpan)(DateTime.Now - cont.usersContext[stepContext.Context.Activity.From.Id].Time);
                if (duration.TotalMinutes > 30)
                {
                    messageText = "Bienvenido al Bot de la \"Sección de Información y Atención Administrativa\" de la Universidad de Extremadura ¿En qué puedo ayudarte?";
                    cont.usersContext[stepContext.Context.Activity.From.Id] = new Context();
                    cont.usersContext[stepContext.Context.Activity.From.Id].Time = DateTime.Now;
                }
                else
                {
                    messageText = stepContext.Options?.ToString() ?? " ";
                    cont.usersContext[stepContext.Context.Activity.From.Id].Time = DateTime.Now;
                }
            }

            // Use the text provided in FinalStepAsync or the default if it is the first time
            messageText = stepContext.Options?.ToString() ?? "Bienvenido al Bot de la \"Sección de Información y Atención Administrativa\" de la Universidad de Extremadura ¿En qué puedo ayudarte?";
            var promptMessage = MessageFactory.Text(messageText, messageText, InputHints.ExpectingInput);
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);
        }


        //Check if the question is covered, if it is valid using the threshold, and send the log to the server
        private async Task<DialogTurnResult> ActStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            prediction = false;
            // Call LUIS for NLP
            var luisResult = await _luisRecognizer.RecognizeAsync<SIAA>(stepContext.Context, cancellationToken);
            double score = luisResult.TopIntent().score;

            //Check the threshold and the intent returned by LUIS
            if (Enum.IsDefined(typeof(SIAA.Intent), luisResult.TopIntent().intent) && luisResult.TopIntent().intent != SIAA.Intent.None && score > 0.6938216175)
            {
                cont.usersContext[stepContext.Context.Activity.From.Id].Intent = $"{luisResult.TopIntent().intent}";

                LogDocument l = new LogDocument($"{luisResult.TopIntent().intent}", stepContext.Context.Activity.From.Id,
                    cont.contexto[$"{luisResult.TopIntent().intent}"], stepContext.Context.Activity.ChannelId, DateTime.Now);

                mongoAPI.postLog(l);

                //If the intent is valid, it is able for a prediction
                prediction = true;

                intent = $"{luisResult.TopIntent().intent}";
            }
            else
            {
                intent = "None";
                var didntUnderstandMessageText = "Lo siento, no puedo ayudarte con eso ahora mismo.";

                var didntUnderstandMessage = MessageFactory.Text(didntUnderstandMessageText, didntUnderstandMessageText, InputHints.IgnoringInput);
                await stepContext.Context.SendActivityAsync(didntUnderstandMessage, cancellationToken);
            }
            return await stepContext.NextAsync(null, cancellationToken);
        }

        //It answer the question relative to the previous intent with the corresponding dialog
        public async Task<DialogTurnResult> AnswerAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if (intent != "None")
            {
                switch (cont.contexto[intent])
                {
                    case "ACF":
                        cont.usersContext[stepContext.Context.Activity.From.Id].Dialog = nameof(ACFDialog);
                        return await stepContext.BeginDialogAsync(nameof(ACFDialog), cont, cancellationToken);
                        break;
                    case "EBAU":
                        cont.usersContext[stepContext.Context.Activity.From.Id].Dialog = nameof(EBAUDialog);
                        return await stepContext.BeginDialogAsync(nameof(EBAUDialog), cont, cancellationToken);
                        break;
                    case "BECA":
                        cont.usersContext[stepContext.Context.Activity.From.Id].Dialog = nameof(BecasDialog);
                        return await stepContext.BeginDialogAsync(nameof(BecasDialog), cont, cancellationToken);
                        break;
                    case "COMPENSACION":
                        cont.usersContext[stepContext.Context.Activity.From.Id].Dialog = nameof(CompensacionDialog);
                        return await stepContext.BeginDialogAsync(nameof(CompensacionDialog), cont, cancellationToken);
                        break;
                    case "CONTINUACION":
                        cont.usersContext[stepContext.Context.Activity.From.Id].Dialog = nameof(ContinuacionDialog);
                        return await stepContext.BeginDialogAsync(nameof(ContinuacionDialog), cont, cancellationToken);
                        break;
                    case "MATRICULA":
                        cont.usersContext[stepContext.Context.Activity.From.Id].Dialog = nameof(MatriculaDialog);
                        return await stepContext.BeginDialogAsync(nameof(MatriculaDialog), cont, cancellationToken);
                        break;
                    case "M25PAU":
                        cont.usersContext[stepContext.Context.Activity.From.Id].Dialog = nameof(PAU25Dialog);
                        return await stepContext.BeginDialogAsync(nameof(PAU25Dialog), cont, cancellationToken);
                        break;
                    case "M40PAU":
                        cont.usersContext[stepContext.Context.Activity.From.Id].Dialog = nameof(PAU40Dialog);
                        return await stepContext.BeginDialogAsync(nameof(PAU40Dialog), cont, cancellationToken);
                        break;
                    case "M45PAU":
                        cont.usersContext[stepContext.Context.Activity.From.Id].Dialog = nameof(PAU45Dialog);
                        return await stepContext.BeginDialogAsync(nameof(PAU45Dialog), cont, cancellationToken);
                        break;
                    case "PREINSCRIPCION":
                        cont.usersContext[stepContext.Context.Activity.From.Id].Dialog = nameof(PreinscripcionDialog);
                        return await stepContext.BeginDialogAsync(nameof(PreinscripcionDialog), cont, cancellationToken);
                        break;
                    case "SIMULTANEIDAD":
                        cont.usersContext[stepContext.Context.Activity.From.Id].Dialog = nameof(SimultaneidadDialog);
                        return await stepContext.BeginDialogAsync(nameof(SimultaneidadDialog), cont, cancellationToken);
                        break;
                    case "TFG":
                        cont.usersContext[stepContext.Context.Activity.From.Id].Dialog = nameof(TFGDialog);
                        return await stepContext.BeginDialogAsync(nameof(TFGDialog), cont, cancellationToken);
                        break;
                    //Check if the context able us to answer the "DEPENDIENTE" question
                    case "DEPENDIENTE":
                        if (cont.usersContext[stepContext.Context.Activity.From.Id].Dialog != null)
                        {
                            return await stepContext.BeginDialogAsync(cont.usersContext[stepContext.Context.Activity.From.Id].Dialog, cont, cancellationToken);
                        }
                        else
                        {
                            var res = "Lo siento, no puedo ayudarte en ese tema ahora mismo.";

                            var m = MessageFactory.Text(res, res, InputHints.IgnoringInput);
                            await stepContext.Context.SendActivityAsync(m, cancellationToken);
                            return await stepContext.NextAsync(null, cancellationToken);
                        }
                        break;
                    //End of the conversation
                    case "NO":
                        var respuesta = cont.usersContext[stepContext.Context.Activity.From.Id].answer();

                        var mensaje = MessageFactory.Text(respuesta, respuesta, InputHints.IgnoringInput);
                        await stepContext.Context.SendActivityAsync(mensaje, cancellationToken);

                        cont.usersContext.Remove(stepContext.Context.Activity.From.Id);

                        return await stepContext.ReplaceDialogAsync(nameof(SatisfaccionDialog), cont, cancellationToken);

                        break;
                    case "AYUDA":
                    case "HOLA":
                        var promptMessage = cont.usersContext[stepContext.Context.Activity.From.Id].answer();
                        return await stepContext.ReplaceDialogAsync(InitialDialogId, promptMessage, cancellationToken);
                        break;
                    default:
                        var didntUnderstandMessageText = cont.usersContext[stepContext.Context.Activity.From.Id].answer();

                        var didntUnderstandMessage = MessageFactory.Text(didntUnderstandMessageText, didntUnderstandMessageText, InputHints.IgnoringInput);
                        await stepContext.Context.SendActivityAsync(didntUnderstandMessage, cancellationToken);
                        return await stepContext.NextAsync(null, cancellationToken);
                        break;
                }
            }
            else
            {
                return await stepContext.NextAsync(null, cancellationToken);
            }
        }



        
        //If the previous intent is valid, the prediction is able, so it call the predictionAPI for the next intent
        //Send the log to server
        private async Task<DialogTurnResult> PredictionAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if (prediction)
            {
                intent = mongoAPI.getNext(intent);
                if (intent != "None")
                {
                    cont.usersContext[stepContext.Context.Activity.From.Id].Intent = intent;

                    cont.usersContext[stepContext.Context.Activity.From.Id].Intent = intent;

                    LogDocument l = new LogDocument(intent, stepContext.Context.Activity.From.Id,
                        cont.contexto[intent], stepContext.Context.Activity.ChannelId, DateTime.Now);

                    mongoAPI.postPrediction(l);
                }
            }
            else
            {
                intent = "None";
            }
            return await stepContext.NextAsync(null, cancellationToken);
        }



        //End of conversation turn
        private async Task<DialogTurnResult> FinalStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            // Restart the main dialog with a different message the second time around
            var promptMessage = "¿Hay algo más que pueda hacer por tí?";
            return await stepContext.ReplaceDialogAsync(InitialDialogId, promptMessage, cancellationToken);
        }
    }
}
