using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;

namespace Microsoft.BotBuilderSamples.Dialogs
{
    public class PAU40Dialog : CancelAndHelpDialog
    {
        // Dependency injection uses this constructor to instantiate the dialog
        public PAU40Dialog()
            : base(nameof(PAU40Dialog))
        {
            AddDialog(new TextPrompt(nameof(TextPrompt)));
            AddDialog(new ConfirmPrompt(nameof(ConfirmPrompt)));
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                WelcomeStepAsync,
            }));

            // The initial child Dialog to run.
            InitialDialogId = nameof(WaterfallDialog);
        }

        //Check if actual intent is "DEPENDIENTE" to change his value, and after of that answer the question
        private async Task<DialogTurnResult> WelcomeStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            string answer;

            var cont = (ContextDictionary)stepContext.Options;

            if (cont.contexto[cont.usersContext[stepContext.Context.Activity.From.Id].Intent] == "DEPENDIENTE")
            {
                switch (cont.usersContext[stepContext.Context.Activity.From.Id].Intent)
                {
                    case "AQUETITULACIONES":
                        cont.usersContext[stepContext.Context.Activity.From.Id].Intent = "M40PAU01";
                        answer = cont.usersContext[stepContext.Context.Activity.From.Id].answer();
                        break;
                    case "RECLAMAR":
                        cont.usersContext[stepContext.Context.Activity.From.Id].Intent = "M40PAU03";
                        answer = cont.usersContext[stepContext.Context.Activity.From.Id].answer();
                        break;
                    default:
                        answer = "No puedo ayudarte con esa pregunta en este momento";
                        break;
                }
            }else{
                answer = cont.usersContext[stepContext.Context.Activity.From.Id].answer();
            }

            var promptMessage = MessageFactory.Text(answer, answer, InputHints.ExpectingInput);
            await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions { Prompt = promptMessage }, cancellationToken);

            return await stepContext.EndDialogAsync(null, cancellationToken);
        }

    }
}
