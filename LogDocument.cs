using System;
using System.Collections;
using System.Collections.Generic;

namespace Microsoft.BotBuilderSamples
{   
    //Class created to store log data
    public class LogDocument
    {
        public string Intent { get; set; }
        public string Username { get; set; }
        public string Channel { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public string Dialog { get; set; }


        public LogDocument(string _intent, string _username, string _dialog, string _channel, DateTimeOffset _timestamp)
        {
            Dialog=_dialog;
            Intent=_intent;
            Username=_username;
            Channel=_channel;
            Timestamp=_timestamp;
        }
    }
}
