using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.AI.Luis;

namespace Microsoft.BotBuilderSamples
{
    public partial class SIAA: IRecognizerConvert
    {
        public string Text;
        public string AlteredText;
        
        public List<string> matricula = new List<string>() { "M00", "M01", "M02", "M03", "M04", "M05", "M06", "M07", "M08", "M09", "M10", "M11", "M12", "M13"};

        public List<string> depCont = new List<string>() { "PLAZOS"};
        
        public enum Intent {
            ACF00,
            ACF04,
            ACF01,
            ACF02,
            ACF03,
            AV00,
            AV01,
            AV02,
            EBAU00,
            EBAU01,
            EBAU02,
            EBAU03,
            EBAU04,
            EBAU05,
            EBAU06,
            EBAU07,
            EBAU08,
            EBAU09,
            EBAU10,
            EBAU11,
            EBAU12,
            EBAU13,
            EBAU14,
            EBAU15,
            EBAU16,
            EBAU17,
            EBAU18,
            M25PAU00,
            M25PAU01,
            M25PAU02,
            M25PAU03,
            M25PAU04,
            M25PAU05,
            M25PAU06,
            M25PAU07,
            M40PAU00,
            M40PAU01,
            M40PAU02,
            M40PAU03,
            M45PAU00,
            M45PAU01,
            M45PAU02,
            M45PAU03,
            AL00,
            BEC00,
            BEC01,
            BEC02,
            BEC03,
            BEC04,
            BEC05,
            BEC07,
            BEC09,
            BEC06,
            BEC08,
            TA00,
            TA01,
            TA02,
            TA03,
            TA04,
            TA06,
            TA07,
            CE00,
            CE01,
            CE02,
            CV00,
            CV01,
            CV02,
            CV03,
            CV04,
            CET00,
            CET01,
            CET02,
            CET03,
            CET04,
            CET05,
            CC00,
            CC01,
            CC02,
            CC03,
            CC04,
            CC05,
            CC06,
            CC07,
            CC08,
            CC09,
            CPE00,
            CPE01,
            CPE02,
            EE00,
            EE01,
            EE02,
            AAAAAAATA05,
            CPE03,
            EE03,
            EE04,
            EE05,
            EE06,
            EE07PE10,
            EE08,
            EE09,
            EE10,
            EE11,
            EE12,
            EE13,
            ID00,
            ID02,
            ID01,
            ID03,
            ID04,
            ID05,
            ID06,
            ID07,
            M00,
            M01,
            M02,
            M03,
            M04,
            M05,
            M06,
            M07,
            M08,
            M09,
            M10,
            M11,
            M12,
            M13,
            M14,
            NC00,
            PE00,
            PE01,
            PE02,
            PE03,
            PE04,
            PE05,
            PE06,
            PE07,
            PE08,
            PE09,
            PE11,
            PE12,
            PRE00,
            PRE01,
            PRE02,
            PRE03,
            PRE04,
            PRE05,
            PRE06,
            PRE08,
            PRE09,
            PRE10,
            PRE11,
            PRE12,
            PRE13,
            SE00,
            SE01,
            SE02,
            SE03,
            SE04,
            TFG00,
            TFG01,
            TFG02,
            TFG03,
            TFG04,
            EPPP00,
            Preguntas,
            BECA,
            MATRICULA,
            ALOJAMIENTO,
            TASAS,
            ACCESOUNI,
            COMPENSACION,
            CVIRTUAL,
            IDIOMAS,
            TFG,
            PREINSCRIPCION,
            EMPLEO,
            PLAZOS,
            REQUISITOS,
            DONDESOLICITAR,
            COMOES,
            CUANDOSECELEBRAN,
            RECLAMAR,
            AQUETITULACIONES,
            CALCULARNOTA,
            DOCUMENTACION,
            CRITERIOS,
            HOLA,
            NO,
            AYUDA,
            None
        };
        public Dictionary<Intent, IntentScore> Intents;

        public class _Entities
        { }
        
        public _Entities Entities;

        [JsonExtensionData(ReadData = true, WriteData = true)]
        public IDictionary<string, object> Properties {get; set; }

        public void Convert(dynamic result)
        {
            var app = JsonConvert.DeserializeObject<SIAA>(JsonConvert.SerializeObject(result, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));
            Text = app.Text;
            AlteredText = app.AlteredText;
            Intents = app.Intents;
            Entities = app.Entities;
            Properties = app.Properties;
        }

        public (Intent intent, double score) TopIntent()
        {
            Intent maxIntent = Intent.None;
            var max = 0.0;
            foreach (var entry in Intents)
            {
                if (entry.Value.Score > max)
                {
                    maxIntent = entry.Key;
                    max = entry.Value.Score.Value;
                }
            }
            return (maxIntent, max);
        }
    }
}
