using System.Collections;
using System.Collections.Generic;

namespace Microsoft.BotBuilderSamples
{
    public class ContextDictionary
    {
        private static ContextDictionary _instance = null;
        public Dictionary<string, Context> usersContext;
        public ArrayList questions;
        public Dictionary<string, string> contexto;

        //Loads the answers from CSV file, and creates the context dictionary
        public ContextDictionary()
        {
            usersContext = new Dictionary<string, Context>();

            questions = new ArrayList();

            string path = @"D:\home\site\wwwroot\Preguntas.csv";

            string[] lines = System.IO.File.ReadAllLines(path);
            contexto = new Dictionary<string, string>();
            foreach (string line in lines)
            {
                ArrayList a = new ArrayList();
                string[] columns = line.Split('#');
                foreach (string column in columns)
                {
                    a.Add(column);
                }
                contexto.Add($"{a[0]}", $"{a[3]}");
                questions.Add(a);
            }

        }

        public static ContextDictionary Instance
        {
            get
            {
                // The first call will create the one and only instance.
                if (_instance == null)
                {
                    _instance = new ContextDictionary();
                }

                // Every call afterwards will return the single instance created above.
                return _instance;
            }
        }
    }
}
