using System;
using System.Collections;
using System.Collections.Generic;

namespace Microsoft.BotBuilderSamples
{
    public class Context
    {
        public string Intent { get; set; }

        public string Dialog { get; set; }
        
        public DateTimeOffset Time { get; set; }


        public Context()
        {
            Dialog=null;
            Intent=null;
        }

        //Answers the question related to the current intent
        public string answer()
        {
            ContextDictionary c = ContextDictionary.Instance;
            bool fin = false;
            ArrayList answer = null;
            for (int i = 0; i < c.questions.Count && !fin; i++)
            {
                ArrayList a = (ArrayList)c.questions[i];
                
                if (a[0].Equals(Intent))
                {
                    fin = true;
                    answer = a;
                }
            }

            //Returns the answer's text
            string didntUnderstandMessageText = "";

            if (fin)
            {
                didntUnderstandMessageText = $"{answer[2]}";
            }
            else
            {
                didntUnderstandMessageText = $"No puedo ayudarte en este tema. Para más información visita la página de 'Preguntas frecuentes' de la UEx";
            }

            return didntUnderstandMessageText;
        }
    }
}
