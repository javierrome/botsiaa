using System;
using System.Text;
using MongoDB.Bson;
using System.Net.Http;
using System.Net;

namespace Microsoft.BotBuilderSamples
{
    //Class created to make API calls
    public static class mongoAPI
    {

        //It create the document with the log data and make the POST to the server
        public static void postLog(LogDocument logDocument)
        {

            var document = new BsonDocument
            {
                {"Intent", logDocument.Intent},
                {"User", logDocument.Username},
                {"Dialog", logDocument.Dialog},
                {"Timestamp", logDocument.Timestamp.ToString()},
                {"Date", logDocument.Timestamp.Date.ToString("d")},
                {"Hour", logDocument.Timestamp.Hour.ToString()},
                {"Month", logDocument.Timestamp.Month.ToString()},
                {"Channel", logDocument.Channel},
            };

            string jsonString = document.ToString();
            var client = new HttpClient();


            var response = client.PostAsync(
                "http://qss.unex.es:2076/siaa",
                 new StringContent(jsonString, Encoding.UTF8, "application/json"));

            
        }

        //It create the document with the prediction data and make the POST to the server
        public static void postPrediction(LogDocument logDocument)
        {

            var document = new BsonDocument
            {
                {"Intent", logDocument.Intent},
                {"User", logDocument.Username},
                {"Dialog", logDocument.Dialog},
                {"Timestamp", logDocument.Timestamp.ToString()},
                {"Date", logDocument.Timestamp.Date.ToString("d")},
                {"Hour", logDocument.Timestamp.Hour.ToString()},
                {"Month", logDocument.Timestamp.Month.ToString()},
                {"Channel", logDocument.Channel},
            };

            string jsonString = document.ToString();
            var client = new HttpClient();


            var response = client.PostAsync(
                "http://qss.unex.es:2076/prediction",
                 new StringContent(jsonString, Encoding.UTF8, "application/json"));

            
        }

        //It create the document with the punctuation data and make the POST to the server
        public static void postPunctuation(string username, string punctuation, string channel)
        {

            var document = new BsonDocument
            {
                {"User", username},
                {"Punctuation", punctuation},
                {"Timestamp", DateTime.Now.ToString()},
                {"Date", DateTime.Now.Date.ToString("d")},
                {"Hour", DateTime.Now.Hour.ToString()},
                {"Month", DateTime.Now.Month.ToString()},
                {"Channel", channel},
            };

            string jsonString = document.ToString();
            var client = new HttpClient();


            var response = client.PostAsync(
                "http://qss.unex.es:2076/satisfaction",
                 new StringContent(jsonString, Encoding.UTF8, "application/json"));

            
        }

        //Make the call to the predictionAPI
        public static string getNext(string intent)
        {
            WebClient webClient = new WebClient();
            string result = webClient.DownloadString("http://qss.unex.es:2076/next?intent="+intent);
            return result;
        }
        
    }
}
